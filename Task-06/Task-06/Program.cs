﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(IsNumberEven(1));
            Console.WriteLine(IsNumberEven(2));
            Console.WriteLine(IsNumberEven(3));
            Console.WriteLine(IsNumberEven(4));
            Console.WriteLine(IsNumberEven(5));
        }

        static string IsNumberEven(int number)
        {
            var output = string.Empty;

            if (number % 2 == 0)
            {
                output = "This number is even";
            }
            else
            {
                output = "This number is false";
            }

            return output;
        }
    }
}
